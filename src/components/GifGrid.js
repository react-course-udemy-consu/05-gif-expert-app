import React, {useEffect, useState} from 'react'
import { GifGridItem } from './GifGridItem';

export const GifGrid = ({category}) => {

  const [images, setImages] = useState([]);
  //Esta función useEffect() ejecuta la función getGif() una sola vez cuando el componente
  //es renderizado por primera vez
  useEffect(() => {
    getGif();
  }, [])
  
  const getGif = async() => {
    const url = 'https://api.giphy.com/v1/gifs/search?q=Naruto&limit=10&api_key=G9uCUuUcFzrqRH6HZi5wS6fCXgYEnyvP';
    const respuesta = await fetch(url);
    const { data } = await respuesta.json();

    const gifs = data.map( img => {
      return {
        id: img.id,
        titulo: img.title,
        url: img.images?.downsized_medium.url
      }
    });
    /* console.log(gifs); */
    setImages(gifs);
  }

  return (
    <div>
      <h3>{ category }</h3>
        {
          images.map( img => (
            <GifGridItem
              key={img.id} 
              {...img}
            />
          ))
        }
    </div>
  )
}

export default GifGrid;
