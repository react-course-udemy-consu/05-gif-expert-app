import React from 'react'

export const GifGridItem = ({titulo, url }) => {

    return (
        <div>
            <img src={ url } alt={ titulo }/>
            <p> { titulo} </p>
        </div>
    )
}
