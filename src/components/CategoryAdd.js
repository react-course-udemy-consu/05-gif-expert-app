import React, { useState } from 'react'
import { PropTypes } from 'prop-types';

export const CategoryAdd = ({ setCategories }) => {

  const [inputvalue, setInputvalue] = useState('');

  const handleInputChange = (e) => {
    setInputvalue(e.target.value);
  }

  const handleSubmit = (e) => {
    
    e.preventDefault();
    if ( inputvalue.trim().length > 2 ) {
      setCategories(categ => [...categ, inputvalue]);
      setInputvalue('');
    }
    
  }

  return (
    <form onSubmit={ handleSubmit }>
      <input
        type="text"
        value= { inputvalue }
        onChange={ handleInputChange }/>
    </form>
  )
}

//TAREA, que setCategory sea requerido
CategoryAdd.propTypes = {
  setCategories: PropTypes.func.isRequired
}
